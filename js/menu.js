// MENU ///////////////////////////////////////////////////////////

const menu = document.getElementById("p3-mobile__menu");
const menubtn = document.getElementById("p3-mobile__btn");
let menustate = false;

function slideMenu() {

    menustate = !menustate;

    if (menustate) {
        menubtn.innerHTML = '<i class="fa fa-times" aria-hidden="true"></i>';
        menu.style.left = "0px";
        menu.style.boxShadow = "100px 0px 300px 0px rgba(0,0,0,0.3)";
    }
    else {
        menubtn.innerHTML = '<i class="fa fa-bars" aria-hidden="true"></i>';
        menu.style.left = "-250px";
        menu.style.boxShadow = "0px 0px 00px 0px rgba(0,0,0,0.0)";
    }
}