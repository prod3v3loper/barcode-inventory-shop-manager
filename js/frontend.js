document.addEventListener('DOMContentLoaded', function () {

    fetchProducts();

    const cartItemsElement = document.querySelector('.p3-cart__items');

    const cartItems = [];

    cartItems.forEach(item => {
        const itemElement = document.createElement('li');
        itemElement.innerHTML = `
            <img src="${item.image}" alt="${item.name}">
            <span>${item.name}</span>
        `;
        cartItemsElement.appendChild(itemElement);
    });
});

if (document.querySelector('.p3-account__dropdown form')) {
    document.querySelector('.p3-account__dropdown form').addEventListener('submit', function (event) {
        event.preventDefault();
        const username = document.getElementById('username').value;
        const password = document.getElementById('password').value;
        console.log('Versuche zu loggen mit:', username, password);
    });
}

function fetchProducts() {

    if (document.querySelector('.p3-product__list')) {
        const productList = document.querySelector('.p3-product__list');
        fetch('ajax/fetch.php')
            .then(response => response.json())
            .then(products => {

                productList.innerHTML = ''; // Bereinige die Liste vor dem Hinzufügen neuer Produkte

                products.forEach(product => {
                    const productElement = document.createElement('div');
                    productElement.className = 'p3-product';

                    let imagesHTML = '';
                    product.images.forEach(image => {
                        imagesHTML += `<img src="${image}" alt="Product Image">`;
                    });

                    productElement.innerHTML = `
                    ${imagesHTML}
                    <h3>${product.title}</h3>
                    <p>${product.price} €</p>
                    <button class="p3-btn" onclick="addToCart(${product.id})">Add to Cart</button>
                `;
                    productList.appendChild(productElement);
                });
            })
            .catch(error => {
                console.error('Error fetching products:', error);
            });
    }
}

function addToCart(productId) {

    console.log("Product with ID", productId, "has been added to cart.");
    // Hier könntest du weitere Logik hinzufügen, um das Produkt zum Warenkorb hinzuzufügen.
}
