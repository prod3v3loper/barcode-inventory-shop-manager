// READY ///////////////////////////////////////////////////////////
document.addEventListener('DOMContentLoaded', function () {

    fetchInventory();

    const searchButtons = document.querySelectorAll('.p3-search__btn');
    searchButtons.forEach(button => {
        button.addEventListener('click', function () {
            const input = button.previousElementSibling;
            performSearch(input.value);
        });
    });

    const searchInputs = document.querySelectorAll('.p3-search__input');
    searchInputs.forEach(input => {
        input.addEventListener('input', function () {
            performSearch(input.value);
        });
    });
});

const form = document.getElementById('p3-form');
const barcodeField = document.getElementById('p3-barcode__number');
const barcode = document.getElementById('p3-barcode');
const productList = document.getElementById('p3-product__list');
const responseForm = document.getElementById('p3-response__form');
const responseInventar = document.getElementById('p3-response__inventar');
const noProductsDiv = document.querySelector('.p3-noproducts');
const imagesContainer = document.getElementById('p3-images-container');

// FORM ///////////////////////////////////////////////////////////

function toggleFormVisibility() {

    resetFormForInsert();

    let opener = document.getElementById('p3-product__btn');
    let form = document.getElementById('p3-form__container');
    let inventory = document.getElementById('p3-inventory');

    if (form.style.display === 'none') {
        form.style.display = 'block';
        inventory.style.display = 'none';
        opener.style.margin = '20px 40px';
        opener.innerHTML = '<i class="fa fa-minus" aria-hidden="true"></i> Close New Product';
        barcodeField.focus();
    } else {
        form.style.display = 'none';
        inventory.style.display = 'block';
        opener.style.margin = '20px';
        opener.innerHTML = '<i class="fa fa-plus" aria-hidden="true"></i> Add New Product';
    }
}

function resetFormForInsert() {

    document.getElementById('p3-form').reset();
    document.getElementById('p3-mode').value = 'insert';
    document.getElementById('p3-id').value = '';
    barcode.innerHTML = '';
    imagesContainer.innerHTML = '';
}

// READ ///////////////////////////////////////////////////////////

function fetchInventory() {

    // <div class="p3-product__itemexpire">${product.expire ? product.expire : 'N/A'}</div>
    // <div class="p3-product__itemcat">${product.category}</div>
    // <div class="p3-product__itemquantity">${product.quantity}</div>
    // <div class="p3-product__itemsupplier">${product.supplier}</div>

    fetch('../ajax/fetch.php')
        .then(response => response.json())
        .then(products => {
            if (productList) {
                let output = '<div class="p3-product__item" id="loadingImage" style="display: none;"><div class="p3-product__load"><img src="../img/load.gif" alt="Loading..."></div></div>';
                if (products.length > 0 && noProductsDiv) {
                    noProductsDiv.style.display = 'none';
                    products.forEach(function (product) {
                        generateBarcode(product.barcode);
                        output += `<div class="p3-product__item">
                                    <div class="p3-product__itemnum">#${product.barcode}</div>
                                    <div class="p3-product__itemtitle">${product.title}</div>
                                    <div class="p3-product__itemprice">${product.price} €</div>
                                    <div class="p3-product__itemstock">${product.stock} In Stock</div>

                                    <div class="p3-product__itemcontainer">
                                        <button class="p3-product__itembtn" onclick="loadDetails(${product.id})"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                        <button class="p3-product__itembtn" onclick="confirmDeletion(${product.id})"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </div>
                                </div>`;
                    });
                } else if (noProductsDiv) {
                    noProductsDiv.style.display = 'block';
                }
                productList.innerHTML = output;
            }
        })
        .catch(error => {
            console.error('Error:', error);
            responseInventar.textContent = "Error: Fetching products.";
        });
}

// SAVE ///////////////////////////////////////////////////////////

function submitForm() {

    const mode = document.getElementById('p3-mode').value;
    const actionUrl = mode === 'insert' ? '../ajax/insert.php' : '../ajax/update.php';

    const titleField = document.getElementById('p3-title');
    const priceField = document.getElementById('p3-price');
    const supplierField = document.getElementById('p3-supplier');
    const stockField = document.getElementById('p3-stock');
    const expireField = document.getElementById('p3-expire');
    const categoryField = document.getElementById('p3-category');
    const quantityField = document.getElementById('p3-quantity');
    const imageField = document.getElementById('p3-image');

    if (barcodeField.value.length === 0) {
        responseForm.textContent = "Please scan a barcode before submitting.";
        return;
    }

    const formData = new FormData(form);
    formData.append('barcode', barcodeField.value);
    formData.append('title', titleField.value);
    formData.append('price', priceField.value);
    formData.append('supplier', supplierField.value);
    formData.append('stock', stockField.value);
    formData.append('expire', expireField.value);
    formData.append('category', categoryField.value);
    formData.append('quantity', quantityField.value);
    for (let i = 0; i < imageField.files.length; i++) {
        formData.append('images[]', imageField.files[i]);
    }

    fetch(actionUrl, {
        method: 'POST',
        body: formData
    })
        .then(response => response.json())
        .then(data => {
            responseForm.textContent = data.message;
            if (data.success) {
                form.reset();
                barcode.innerHTML = '';
                fetchInventory();
            }
        })
        .catch(error => {
            console.error('Error:', error);
            responseForm.textContent = "Error: Submit form.";
        });
}

// EDIT ///////////////////////////////////////////////////////////

function loadDetails(productId) {

    toggleFormVisibility();

    document.getElementById('p3-mode').value = 'update';
    document.getElementById('p3-id').value = productId;

    fetch('../ajax/detail.php?productId=' + productId, {
        method: 'GET'
    })
        .then(response => response.json())
        .then(data => {
            responseForm.textContent = data.message;
            if (data.success) {
                document.getElementById('p3-barcode__number').value = data.product.barcode;
                generateBarcode(data.product.barcode);
                document.getElementById('p3-title').value = data.product.title;
                document.getElementById('p3-price').value = data.product.price;
                document.getElementById('p3-supplier').value = data.product.supplier;
                document.getElementById('p3-stock').value = data.product.stock;
                document.getElementById('p3-expire').value = data.product.expire;
                document.getElementById('p3-category').value = data.product.category;
                document.getElementById('p3-quantity').value = data.product.quantity;

                imagesContainer.innerHTML = '';
                data.images.forEach(image => {
                    const imageWrapper = document.createElement('div');
                    imageWrapper.className = 'p3-image__wrapper';
                    imageWrapper.id = 'image-' + image.id;

                    const imageElement = document.createElement('img');
                    imageElement.src = image.imagePath;
                    imageElement.alt = 'Product Image';
                    imageElement.className = 'product-image';

                    const deleteButton = document.createElement('button');
                    deleteButton.textContent = 'Delete';
                    deleteButton.className = 'delete-image-btn';
                    deleteButton.setAttribute('data-image-id', image.id);
                    deleteButton.onclick = function () {
                        deleteImage(image.id); // Funktion zum Löschen des Bildes
                    };

                    imageWrapper.appendChild(imageElement);
                    imageWrapper.appendChild(deleteButton);
                    imagesContainer.appendChild(imageWrapper);
                });
            }
        })
        .catch(error => {
            console.error('Error:', error);
            responseForm.textContent = 'Error: loading product information.';
        });
}

// DELETE ///////////////////////////////////////////////////////////

function confirmDeletion(productId) {
    if (confirm("Are you sure you want to delete this product? All associated images will also be removed.")) {
        deleteProduct(productId);
    }
}

function deleteProduct(productId) {

    fetch('../ajax/delete.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'productId=' + productId
    })
        .then(response => response.json())
        .then(data => {
            responseInventar.textContent = data.message;
            if (data.success) {
                fetchInventory();
            }
        })
        .catch(error => {
            console.error('Error:', error);
            responseInventar.textContent = 'Error: Deleting product.';
        });
}

document.addEventListener('click', function (event) {

    if (event.target.classList.contains('delete-image-btn')) {

        const imageId = event.target.dataset.imageId;
        if (confirm('Are you sure you want to delete this image?')) {
            fetch('../ajax/image.php', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: 'imageId=' + encodeURIComponent(imageId)
            })
                .then(response => response.json())
                .then(data => {
                    if (data.success) {
                        const imageElement = document.getElementById('image-' + imageId);
                        if (imageElement) {
                            imageElement.remove();
                        }
                    } else {
                        alert('Error: ' + data.message);
                    }
                })
                .catch(error => {
                    console.error('Error:', error);
                    alert('Failed to delete the image.');
                });
        }
    }
});

// SEARCH ///////////////////////////////////////////////////////////

function performSearch(searchText) {

    const loadingImage = document.getElementById('loadingImage');

    loadingImage.style.display = 'block';
    noProductsDiv.style.display = 'none';

    if (searchText.trim() === '') {
        fetchInventory();
        return;
    }

    // <div class="p3-product__itemexpire">${product.expire ? product.expire : 'N/A'}</div>
    // <div class="p3-product__itemcat">${product.category}</div>
    // <div class="p3-product__itemquantity">${product.quantity}</div>
    // <div class="p3-product__itemsupplier">${product.supplier}</div>

    fetch('../ajax/search.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'query=' + encodeURIComponent(searchText)
    })
        .then(response => response.json())
        .then(products => {
            setTimeout(() => {
                productList.innerHTML = '<div class="p3-product__item" id="loadingImage" style="display: none;"><div class="p3-product__load"><img src="../img/load.gif" alt="Loading..."></div></div>';
                loadingImage.style.display = 'none';
                noProductsDiv.style.display = 'none';
                if (products.length > 0) {
                    products.forEach(product => {
                        let productHTML = `<div class="p3-product__item">
                                <div class="p3-product__itemnum">#${product.barcode}</div>
                                <div class="p3-product__itemtitle">${product.title}</div>
                                <div class="p3-product__itemprice">${product.price} €</div>
                                <div class="p3-product__itemstock">${product.stock} In Stock</div>
                                <div class="p3-product__itemcontainer">
                                    <button class="p3-product__itembtn" onclick="loadDetails(${product.id})"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                    <button class="p3-product__itembtn" onclick="confirmDeletion(${product.id})"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                </div>
                            </div>`;
                        productList.innerHTML += productHTML;
                    });
                } else {
                    noProductsDiv.style.display = 'block';
                }
            }, 300);
        })
        .catch(error => {
            console.error('Error:', error);
            loadingImage.style.display = 'none';
            responseInventar.textContent = "Error: Searching products.";
        });
}

// BARCODE ///////////////////////////////////////////////////////////

function generateBarcode(barcodeValue) {

    if (typeof JsBarcode !== 'undefined') {
        JsBarcode("#p3-barcode", barcodeValue, {
            format: "CODE128", // Fomats, e.g. CODE128, EAN, UPC, etc.
            lineColor: "#000",
            width: 2,
            height: 40,
            displayValue: true
        });
    }
}

function checkBarcode(event) {

    event.preventDefault();

    if (event.keyCode === 13) {

        if (barcodeField.value.length > 0) {
            barcodeField.select();
            barcodeField.value = barcodeField.value;
            generateBarcode(barcodeField.value);
        }

        fetch('../ajax/check.php', {
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: 'barcode=' + encodeURIComponent(barcodeField.value)
        })
            .then(response => response.json())
            .then(data => {
                responseForm.textContent = data.message;
            })
            .catch(error => {
                console.error('Error:', error);
                responseForm.textContent = "Error: Checking barcode.";
            });
    }
}

if (document.getElementById('p3-image')) {
    document.getElementById('p3-image').addEventListener('change', function () {
        let fileNames = [];
        for (let i = 0; i < this.files.length; i++) {
            fileNames.push(this.files[i].name);
        }
        document.querySelector('.file-name').textContent = fileNames.join(', ') || 'No file chosen';
    });
}