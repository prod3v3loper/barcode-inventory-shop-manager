<header class="p3-header">
    <div class="p3-container">
        <div class="p3-logo">p3 shop</div>
        <nav class="p3-nav">
            <ul class="p3-nav__list">
                <li class="p3-nav__listitem"><a class="p3-nav__link" href="index.php">Home</a></li>
                <li class="p3-nav__listitem"><a class="p3-nav__link" href="product.php">Products</a></li>
                <li class="p3-nav__listitem p3-cart">
                    <a class="p3-nav__link" href="#cart">Cart</a>
                    <div class="p3-cart__dropdown">
                        <h3>p3 Cart</h3>
                        <ul class="p3-cart__items">
                        </ul>
                        <a href="#checkout" class="p3-btn__checkout">Checkout</a>
                    </div>
                </li>
                <li class="p3-nav__listitem p3-account">
                    <a class="p3-nav__link" href="#account">Account</a>
                    <div class="p3-account-dropdown">
                        <h3>p3 Account</h3>
                        <form action="login.php" method="post">
                            <label for="username">Username:</label>
                            <input type="text" id="username" name="username">
                            <label for="password">Password:</label>
                            <input type="password" id="password" name="password">
                            <button type="submit">Login</button>
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
    </div>
</header>