# SIMPLE SHOP SYSTEM

To manage inventory and enter products through a barcode scanner, you need a basic web application consisting of HTML for the user interface and PHP for the server-side logic.

<img src="img/p3-website.png" alt="website" width="424">

<img src="img/p3-inventory.png" alt="inventory" width="424">

<img src="img/p3-barcode.png" alt="barcode" width="424">

You can scan the barcode with a barcode scanner:

[Barcode scanner](https://amzn.to/4b12p4R)

## DONE

- [x] Shop website (Responsive)
- [x] User login
- [x] Inventory management
- [x] Barcode scanning

## TODO

- [ ] Add `.env` file
- [ ] User register
- [ ] User forgot Password
- [ ] Basket
- [ ] Checkout

# SETUP

Call the url `/setup.php` to setup the project.

Login created after the setup:

```
Username: Admin
Password: Admin123#
```

# USED

- Pure HTML
- Pure CSS
- Pure PHP (with MySQLi)
- Pure JS
- Pure AJAX
- Pure JSON

# ISSUE

Please use the issue tab to request a:

* Bug
* Feature

Choose template and report a bug or feature you want [issues](https://gitlab.com/prod3v3loper/barcode-inventory-shop-manager/-/issues).

# CONTRIBUTE

Please read the [contributing](https://gitlab.com/prod3v3loper/barcode-inventory-shop-manager/-/blob/main/CONTRIBUTING.md) to contribute.

# VULNERABILITY

Please use the Security section for privately reporting a [vulnerability](https://gitlab.com/prod3v3loper/barcode-inventory-shop-manager/-/blob/main/.gitlab/SECURITY.md).

# Authors

**[PROD3V3LOPER](https://www.prod3v3loper.com)** - _All works_

# LICENSE

[MIT](https://gitlab.com/prod3v3loper/barcode-inventory-shop-manager/-/blob/main/LICENSE)
