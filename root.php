<?php

/**
 * Get the URL protocol
 * 
 * @return string
 */
function get_protocol()
{
    $HTTPS = (isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : '');
    $protocol = !isset($HTTPS) || $HTTPS != 'on' ? 'http://' : 'https://';
    return $protocol;
}

/**
 * Get Host checks if host exists when exists then returned back
 * 
 * @return mixed
 */
function get_host()
{
    return (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : false);
}

define('PROJECT_DOCUMENT_ROOT', __DIR__);
define('DOCUMENT_ROOT', str_replace(PROJECT_DOCUMENT_ROOT, '', str_replace(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT'), '', str_replace("\\", "/", __DIR__))));
define('PROJECT_HTTP_ROOT', get_protocol() . get_host() . DIRECTORY_SEPARATOR . DOCUMENT_ROOT);
