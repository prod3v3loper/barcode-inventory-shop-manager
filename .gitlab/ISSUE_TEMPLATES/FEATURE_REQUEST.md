## Feature Request

**Contact Details**
How can we get in touch with you if we need more info?

**Description of feature**
Is your feature request related to a problem? A clear and concise description of what the problem is.

**Suggested Solution**
Describe the solution you'd like. A clear and concise description of what you want to happen.

**Alternatives**
Describe alternatives you've considered. A clear and concise description of any alternative solutions or features you've considered.

**Additional Context**
Add any other context about the problem here.

**Code of Conduct**
By submitting this issue, you agree to follow our [Code of Conduct](https://gitlab.com/prod3v3loper/barcode-inventory-shop-manager/-/blob/main/.gitlab/CODE_OF_CONDUCT.md).
- [ ] I agree to follow this project's Code of Conduct.
