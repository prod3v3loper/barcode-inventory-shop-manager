## Bug Report

**Contact Details**
How can we get in touch with you if we need more info?

**What happened?**
What happened? What did you expect to happen?

**Steps to reproduce the behavior**
1. Go to '...'
2. Click on '....'
3. Scroll to '....'
4. See error

**Device and information**
- OS: [e.g. iOS]
- Browser: [e.g. Chrome, Safari]
- Version: [e.g. 22]
- Device: [e.g. iPhone6]

**Relevant log output**
Please copy and paste any relevant log output here.

**Code of Conduct**
By submitting this issue, you agree to follow our [Code of Conduct](https://gitlab.com/prod3v3loper/barcode-inventory-shop-manager/-/blob/main/.gitlab/CODE_OF_CONDUCT.md).
- [ ] I agree to follow this project's Code of Conduct.
