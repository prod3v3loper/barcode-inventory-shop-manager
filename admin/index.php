<?php

session_start();

if (isset($_SESSION['user-logged-in']) && $_SESSION['user-logged-in'] !== true || !isset($_SESSION['user-logged-in'])) {
    header('Location: login.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>p3 - Inventory Manager</title>

    <link rel="stylesheet" type="text/css" href="../css/inventory.css">

    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="../img/p3.png">
    <link rel="icon" type="image/png" href="../img/p3.png" sizes="32x32">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <script src="../js/barcode.js"></script>
</head>

<body>

    <div id="p3-mobile__menu" class="p3-sidenav__mobile">

        <div class="p3-logo">p3</div>

        <form class="p3-searchbar">
            <input type="text" name="search" class="p3-search__input">
            <button type="button" name="search__btn" class="p3-search__btn"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>

        <div class="p3-spacer"></div>

        <a href="inventory.php" class="p3-sidenav__btn"><i class="fa fa-list p3-icon" aria-hidden="true"></i> Products</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-users p3-icon" aria-hidden="true"></i> Accounts</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-cogs p3-icon" aria-hidden="true"></i> Settings</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-calendar p3-icon" aria-hidden="true"></i> Calendar</a>
        <a href="index.php" class="p3-sidenav__btn"><i class="fa fa-user p3-icon" aria-hidden="true"></i> Logout</a>

        <div class="p3-copy">
            <small>All rights reserved | &copy; p3</small>
        </div>

    </div>

    <div class="p3-sidenav">

        <div class="p3-logo">p3</div>

        <div class="p3-spacer"></div>

        <a href="inventory.php" class="p3-sidenav__btn"><i class="fa fa-list p3-icon" aria-hidden="true"></i> Products</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-users p3-icon" aria-hidden="true"></i> Accounts</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-cogs p3-icon" aria-hidden="true"></i> Settings</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-calendar p3-icon" aria-hidden="true"></i> Calendar</a>
        <a href="index.php" class="p3-sidenav__btn"><i class="fa fa-user p3-icon" aria-hidden="true"></i> Logout</a>

        <div class="p3-copy">
            <small>All rights reserved | &copy; p3</small>
        </div>

    </div>

    <div class="p3-main">

        <div class="p3-mobile">
            <div class="p3-logo p3-small-logo">p3</div>
            <div class="p3-mobile__holder">
                <button id="p3-mobile__btn" class="p3-mobile__btn" onclick="slideMenu()"><i class="fa fa-bars" aria-hidden="true"></i></button>
            </div>
        </div>

        <div class="p3-container">
            <?php
            echo "<h1>Welcome to Admin Page</h1>";
            echo "<p>This is a secure area because you are logged in.</p>";
            echo '<a href="logout.php">Logout</a>';
            ?>
        </div>
    </div>

    <script type="text/javascript" src="../js/menu.js"></script>
    <script type="text/javascript" src="../js/backend.js"></script>
</body>

</html>