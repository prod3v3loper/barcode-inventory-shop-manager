<?php

session_start();

if (isset($_SESSION['user-logged-in']) && $_SESSION['user-logged-in'] !== true || !isset($_SESSION['user-logged-in'])) {
    header('Location: /');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>p3 - Inventory Manager</title>

    <link rel="stylesheet" type="text/css" href="../css/inventory.css">

    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="../img/p3.png">
    <link rel="icon" type="image/png" href="../img/p3.png" sizes="32x32">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <script src="../js/barcode.js"></script>
</head>

<body>

    <div id="p3-mobile__menu" class="p3-sidenav__mobile">

        <div class="p3-logo">p3</div>

        <form class="p3-searchbar">
            <input type="text" name="search" class="p3-search__input">
            <button type="button" name="search__btn" class="p3-search__btn"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>

        <div class="p3-spacer"></div>

        <a href="#" class="p3-sidenav__btn"><i class="fa fa-list p3-icon" aria-hidden="true"></i> Products</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-users p3-icon" aria-hidden="true"></i> Accounts</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-cogs p3-icon" aria-hidden="true"></i> Settings</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-calendar p3-icon" aria-hidden="true"></i> Calendar</a>
        <a href="index.php" class="p3-sidenav__btn"><i class="fa fa-user p3-icon" aria-hidden="true"></i> Logout</a>

        <div class="p3-copy">
            <small>All rights reserved | &copy; p3</small>
        </div>

    </div>

    <div class="p3-sidenav">

        <div class="p3-logo">p3</div>

        <form class="p3-searchbar">
            <input type="text" name="search" class="p3-search__input">
            <button type="button" name="search__btn" class="p3-search__btn"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>

        <div class="p3-spacer"></div>

        <a href="#" class="p3-sidenav__btn"><i class="fa fa-list p3-icon" aria-hidden="true"></i> Products</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-users p3-icon" aria-hidden="true"></i> Accounts</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-cogs p3-icon" aria-hidden="true"></i> Settings</a>
        <a href="#" class="p3-sidenav__btn"><i class="fa fa-calendar p3-icon" aria-hidden="true"></i> Calendar</a>
        <a href="index.php" class="p3-sidenav__btn"><i class="fa fa-user p3-icon" aria-hidden="true"></i> Logout</a>

        <div class="p3-copy">
            <small>All rights reserved | &copy; p3</small>
        </div>

    </div>

    <div class="p3-main">

        <div class="p3-mobile">
            <div class="p3-logo p3-small-logo">p3</div>
            <div class="p3-mobile__holder">
                <button id="p3-mobile__btn" class="p3-mobile__btn" onclick="slideMenu()"><i class="fa fa-bars" aria-hidden="true"></i></button>
            </div>
        </div>

        <button id="p3-product__btn" onclick="toggleFormVisibility()"><i class="fa fa-plus" aria-hidden="true"></i> Add New Product</button>

        <div class="p3-form__container" id="p3-form__container" style="display:none;">
            <h1>Add to Inventory</h1>

            <div id="p3-response__form"></div>

            <form id="p3-form">

                <input type="hidden" id="p3-mode" name="p3-mode" value="insert">
                <input type="hidden" id="p3-id" name="p3-id" value="">

                <label for="p3-barcode__number">Barcode:</label>
                <input type="text" id="p3-barcode__number" name="barcode" required autofocus onkeyup="checkBarcode(event)">

                <svg id="p3-barcode"></svg>

                <label for="p3-image" class="label-for-image"><i class="fa fa-upload"></i> Upload Product Images</label>
                <input type="file" id="p3-image" name="images[]" multiple><br>
                <span class="file-name">No file chosen</span><br><br>

                <div id="p3-images-container"></div>

                <div style="clear:both;"></div>

                <label for="p3-title">Titel:</label>
                <input type="text" id="p3-title" name="title" required>

                <label for="p3-price">Price:</label>
                <input type="number" id="p3-price" name="price" step="0.01" required>

                <label for="p3-supplier">Supplier:</label>
                <input type="text" id="p3-supplier" name="supplier">

                <label for="p3-stock">Stock:</label>
                <input type="number" id="p3-stock" name="stock">

                <label for="p3-expire">Expiry Date:</label>
                <input type="date" id="p3-expire" name="expire">

                <label for="p3-category">Category:</label>
                <input type="text" id="p3-category" name="category">

                <label for="p3-quantity">Quantity:</label>
                <input type="number" id="p3-quantity" name="quantity" required>

                <button type="button" name="p3-send" onclick="submitForm()">Add</button>

            </form>
        </div>

        <div id="p3-inventory">

            <div class="p3-inventory__title">Inventory</div>
            <div class="p3-inventory__desc">View and manage your stock</div>

            <div id="p3-response__inventar"></div>

            <div id="p3-product__list">
                <div class="p3-product__item" id="loadingImage" style="display: none;">
                    <div class="p3-product__load">
                        <img src="../img/load.gif" alt="Loading...">
                    </div>
                </div>
            </div>

            <div class="p3-noproducts" style="display: none;">
                <div class="p3-product__item">
                    <div class="p3-noproducts__message">No products available.</div>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript" src="../js/menu.js"></script>
    <script type="text/javascript" src="../js/backend.js"></script>
</body>

</html>