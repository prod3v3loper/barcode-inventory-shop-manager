<?php

session_start();

if (isset($_SESSION['user-logged-in']) && $_SESSION['user-logged-in'] === true) {
    unset($_SESSION['user-logged-in']);
    header('Location: ../login.php');
}
