<?php

require_once 'root.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'config.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'error.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'db/conn.php';

echo '<h1>PROJECT SETUP</h1>';
echo '<p>Weclome to p3 Inventory Manager with barcodes by <a href="https://www.prod3v3loper.com" target="_blank">prod3v3loper</a>.<br><br></p>';

echo '<h2>Create Database</h2>';

$createUserTable = "CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    role ENUM('admin', 'user') DEFAULT 'user',
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)";

$createProductTable = "CREATE TABLE IF NOT EXISTS product (
    id INT AUTO_INCREMENT PRIMARY KEY,
    barcode VARCHAR(255) NOT NULL UNIQUE,
    title VARCHAR(255) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    supplier VARCHAR(255),
    stock INT,
    expire DATE,
    category VARCHAR(255),
    quantity INT DEFAULT 0,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)";

$createPicTable = "CREATE TABLE IF NOT EXISTS pic (
    id INT AUTO_INCREMENT PRIMARY KEY,
    productID INT,
    imageID VARCHAR(255) NOT NULL,
    FOREIGN KEY (productID) REFERENCES product(id) ON DELETE CASCADE
)";

if ($conn->query($createUserTable) === TRUE) {
    echo "<p>Table <b>users</b> created successfully or already exists.<br></p>";
} else {
    echo "<p>Error creating table <b>users</b>: " . $conn->error . "<br></p>";
}

// Insert Admin User
$adminUsername = 'Admin';
$adminPassword = password_hash('Admin123#', PASSWORD_DEFAULT);
$insertAdmin = "INSERT INTO users (username, password, role) VALUES (?, ?, 'admin') ON DUPLICATE KEY UPDATE username = VALUES(username)";

$stmt = $conn->prepare($insertAdmin);
if (!$stmt) {
    echo "<p>Error preparing statement to insert admin: " . $conn->error . "</p>";
} else {
    $stmt->bind_param("ss", $adminUsername, $adminPassword);
    if ($stmt->execute()) {
        echo "<p>Admin user created or already exists.<br></p>";
    } else {
        echo "<p>Error inserting admin user: " . $stmt->error . "</p>";
    }
    $stmt->close();
}

if ($conn->query($createProductTable) === TRUE) {
    echo "<p>Table <b>product</b> created successfully or already exists.<br></p>";
} else {
    echo "<p>Error creating table <b>product</b>: " . $conn->error . "<br></p>";
}

if ($conn->query($createPicTable) === TRUE) {
    echo "<p>Table <b>pic</b> created successfully or already exists.<br></p>";
} else {
    echo "<p>Error creating table <b>pic</b>: " . $conn->error . "<br></p>";
}

echo '<h3>Create Upload directory</h3>';

$uploadDir = "upload/";

if (!file_exists($uploadDir)) {
    if (mkdir($uploadDir, 0755, true)) {
        echo '<p>Upload directory created successfully <b>' . $uploadDir . '</b>.<br></p>';
    } else {
        echo '<p>Error creating Upload directory <b>' . $uploadDir . '</b>.<br></p>';
    }
} else {
    echo '<p>Upload directory already exists <b>' . $uploadDir . '</b>.<br></p>';
}

if (file_exists($uploadDir) && !is_writable($uploadDir)) {
    echo '<p>Upload directory is not writable.<br></p>';
} else {
    echo '<p>Upload directory is writable.<br></p>';
}

echo '<h4>Needed Permissions</h4>';

if (!chmod($uploadDir, 0755)) {
    echo '<p>Error setting permissions for <b>' . $uploadDir . '</b>.</p>';
} else {
    echo '<p>Permissions successfully set for <b>' . $uploadDir . '</b>.</p>';
}

$permissionFiles = ["ajax/insert.php", "ajax/update.php"];

foreach ($permissionFiles as $file) {
    if (!chmod($file, 0655)) {
        echo '<p>Error setting permissions for <b>' . $file . '</b>.</p>';
    } else {
        echo '<p>Permissions successfully set for <b>' . $file . '</b>.</p>';
    }
}
