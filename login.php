<?php

session_start();

require_once 'root.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'config.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'error.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'db/conn.php';

$message = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $username = $_POST['username'] ?? '';
    $password = $_POST['password'] ?? '';

    if (empty($username) || empty($password)) {
        $message = 'Username and Password are required.';
    } else {

        $sql = "SELECT id, username, password FROM users WHERE username = ?";
        $stmt = $conn->prepare($sql);
        if ($stmt) {
            $stmt->bind_param('s', $username);
            $stmt->execute();
            $result = $stmt->get_result();
            if ($result->num_rows === 1) {
                $user = $result->fetch_assoc();
                if (password_verify($password, $user['password'])) {
                    $_SESSION['user-logged-in'] = true;
                    $_SESSION['user-id'] = $user['id'];
                    header('Location: admin');
                    exit;
                } else {
                    $message = 'Invalid username or password.';
                }
            } else {
                $message = 'Invalid username or password.';
            }
            $stmt->close();
        } else {
            $message = 'Database error: ' . $conn->error;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>p3 - Online Shop</title>

    <link rel="stylesheet" href="css/style.css">

    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="img/p3.png">
    <link rel="icon" type="image/png" href="img/p3.png" sizes="32x32">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
</head>

<body>

    <div id="p3-mobile__menu" class="p3-sidenav__mobile">
        <div class="p3-logo">p3</div>
        <form class="p3-searchbar">
            <input type="text" name="search" class="p3-search__input">
            <button type="button" name="search__btn" class="p3-search__btn"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
        <div class="p3-spacer"></div>
        <button class="p3-sidenav__btn"><i class="fa fa-list p3-icon" aria-hidden="true"></i> Products</button>
        <button class="p3-sidenav__btn"><i class="fa fa-users p3-icon" aria-hidden="true"></i> Account</button>
        <button class="p3-sidenav__btn"><i class="fa fa-cogs p3-icon" aria-hidden="true"></i> Settings</button>
        <button class="p3-sidenav__btn"><i class="fa fa-calendar p3-icon" aria-hidden="true"></i> Calendar</button>
    </div>

    <header class="p3-header">
        <div class="p3-container">
            <div class="p3-logo">p3</div>
            <nav class="p3-nav">
                <ul class="p3-nav__list">
                    <li class="p3-nav__listitem"><a class="p3-nav__link" href="index.php">Home</a></li>
                    <li class="p3-nav__listitem"><a class="p3-nav__link" href="product.php">Products</a></li>
                    <li class="p3-nav__listitem p3-cart">
                        <a class="p3-nav__link" href="#cart">Cart</a>
                        <div class="p3-cart__dropdown">
                            <h3>p3 Cart</h3>
                            <ul class="p3-cart__items">
                            </ul>
                            <a href="#checkout" class="p3-btn__checkout">Checkout</a>
                        </div>
                    </li>
                    <li class="p3-nav__listitem p3-account">
                        <a class="p3-nav__link" href="#account">Account</a>
                        <div class="p3-account-dropdown">
                            <h3>p3 Account</h3>
                            <form action="login.php" method="post">
                                <label for="username">Username:</label>
                                <input type="text" id="username" name="username">
                                <label for="password">Password:</label>
                                <input type="password" id="password" name="password">
                                <button type="submit">Login</button>
                            </form>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <div class="p3-mobile">
        <div class="p3-logo p3-small-logo">p3</div>
        <div class="p3-mobile__holder">
            <button id="p3-mobile__btn" class="p3-mobile__btn" onclick="slideMenu()"><i class="fa fa-bars" aria-hidden="true"></i></button>
        </div>
    </div>

    <main class="p3-main">

        <section class="p3-section" id="p3-home">
            <div class="p3-container">
                <div class="p3-login">
                    <h2>Login</h2>
                    <?php
                    echo $message;
                    ?>
                    <br><br>
                    <form action="login.php" method="POST">
                        <div class="p3-form-group">
                            <label for="username">Username:</label>
                            <input type="text" id="username" name="username">
                        </div>
                        <div class="p3-form-group">
                            <label for="password">Password:</label>
                            <input type="password" id="password" name="password">
                        </div>
                        <button type="submit">Login</button>
                    </form>
                </div>
            </div>
        </section>

    </main>

    <footer class="p3-footer">
        <div class="p3-container">
            <p>p3 Shop © 2024</p>
        </div>
    </footer>

    <script type="text/javascript" src="js/menu.js"></script>
    <script type="text/javascript" src="js/frontend.js"></script>
</body>

</html>