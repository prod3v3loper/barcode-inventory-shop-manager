<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>p3 - Online Shop</title>

    <link rel="stylesheet" href="css/style.css">

    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="img/p3.png">
    <link rel="icon" type="image/png" href="img/p3.png" sizes="32x32">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
</head>

<body>

    <div id="p3-mobile__menu" class="p3-sidenav__mobile">
        <div class="p3-logo">p3</div>
        <form class="p3-searchbar">
            <input type="text" name="search" class="p3-search__input">
            <button type="button" name="search__btn" class="p3-search__btn"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
        <div class="p3-spacer"></div>
        <button class="p3-sidenav__btn"><i class="fa fa-list p3-icon" aria-hidden="true"></i> Products</button>
        <button class="p3-sidenav__btn"><i class="fa fa-users p3-icon" aria-hidden="true"></i> Account</button>
        <button class="p3-sidenav__btn"><i class="fa fa-cogs p3-icon" aria-hidden="true"></i> Settings</button>
        <button class="p3-sidenav__btn"><i class="fa fa-calendar p3-icon" aria-hidden="true"></i> Calendar</button>
    </div>

    <?php require_once 'header.php'; ?>

    <div class="p3-mobile">
        <div class="p3-logo p3-small-logo">p3</div>
        <div class="p3-mobile__holder">
            <button id="p3-mobile__btn" class="p3-mobile__btn" onclick="slideMenu()"><i class="fa fa-bars" aria-hidden="true"></i></button>
        </div>
    </div>

    <main class="p3-main">

        <section class="p3-section" id="p3-home">
            <div class="p3-container">
                <div class="p3-jumbotron">
                    <h1>Welcome to p3 online shop</h1>
                    <p>Find the best products here.</p>
                    <a href="login.php" role="button">Get Started</a>
                </div>
            </div>
        </section>

        <section class="p3-section" id="p3-products">
            <div class="p3-container">
                <h2>Products</h2>
                <div class="p3-product__list">
                </div>
            </div>
        </section>

    </main>

    <footer class="p3-footer">
        <div class="p3-container">
            <p>p3 Shop © 2024</p>
        </div>
    </footer>

    <script type="text/javascript" src="js/menu.js"></script>
    <script type="text/javascript" src="js/frontend.js"></script>
</body>

</html>