<?php

header('Content-Type: application/json');

require_once '../root.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'config.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'error.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'db/conn.php';

$query = isset($_POST['query']) && !empty($_POST['query']) ? $_POST['query'] : '';

if ($query) {

    $sql = "SELECT barcode, title, price, supplier, stock, expire, category, quantity FROM product WHERE title LIKE ?";

    $stmt = $conn->prepare($sql);
    if (!$stmt) {
        echo json_encode(['error' => 'Error preparing statement: ' . $conn->error]);
        exit;
    }

    $likeQuery = "%" . $query . "%";
    $stmt->bind_param("s", $likeQuery);
    if (!$stmt->execute()) {
        echo json_encode(['error' => 'Error executing statement: ' . $stmt->error]);
        exit;
    }

    $result = $stmt->get_result();

    $products = [];
    while ($row = $result->fetch_assoc()) {

        $products[] = [
            'barcode' => htmlspecialchars($row['barcode']),
            'title' => htmlspecialchars($row['title']),
            'price' => htmlspecialchars($row['price']),
            'supplier' => htmlspecialchars($row['supplier']),
            'stock' => htmlspecialchars($row['stock']),
            'expire' => htmlspecialchars($row['expire']),
            'category' => htmlspecialchars($row['category']),
            'quantity' => htmlspecialchars($row['quantity'])
        ];
    }

    echo json_encode($products);

    $stmt->close();
    $conn->close();
} else {

    echo json_encode(['success' => false, 'message' => 'No search query found.']);
}
