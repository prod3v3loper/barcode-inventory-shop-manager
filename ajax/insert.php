<?php

header('Content-Type: application/json');

require_once '../root.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'config.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'error.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'db/conn.php';

$barcode = trim($_POST['barcode'] ?? '');
$title = trim($_POST['title'] ?? '');
$price = trim($_POST['price'] ?? '');
$supplier = trim($_POST['supplier'] ?? '');
$stock = trim($_POST['stock'] ?? '');
$expire = trim($_POST['expire'] ?? '');
$category = trim($_POST['category'] ?? '');
$quantity = intval($_POST['quantity'] ?? 0);
$images = [];

$errors = [];

if (empty($barcode)) {
    $errors[] = "Barcode is required.";
}
if (empty($title)) {
    $errors[] = "Title is required.";
}
if (!empty($price) && !is_numeric($price)) {
    $errors[] = "Price must be a valid number.";
}
if (!empty($stock) && !is_numeric($stock)) {
    $errors[] = "Minimum stock must be a valid number.";
}
if (!empty($expire) && !preg_match("/^\d{4}-\d{2}-\d{2}$/", $expire)) {
    $errors[] = "Expiry date must be in YYYY-MM-DD format.";
}
if ($quantity <= 0) {
    $errors[] = "Quantity must be a non-negative integer.";
}

if (!empty($_FILES['images']['name'][0])) {

    $targetDir = PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . "upload/";
    $allowedTypes = ['jpg', 'jpeg', 'png', 'gif'];

    foreach ($_FILES['images']['name'] as $key => $value) {

        $targetFile = $targetDir . basename($_FILES['images']['name'][$key]);

        if (!in_array($targetFile, $images)) {

            $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
            $check = getimagesize($_FILES['images']['tmp_name'][$key]);

            if ($check !== false && in_array($imageFileType, $allowedTypes)) {
                if (move_uploaded_file($_FILES['images']['tmp_name'][$key], $targetFile)) {
                    $images[] = $value;
                } else {
                    $errors[] = "Error uploading file: " . $targetFile;
                }
            } else {
                $errors[] = "File is not an image or type is not allowed: " . $value;
            }
        }
    }
} else {
    $errors[] = "At least one image file is required.";
}

if (count($errors) > 0) {
    echo json_encode(["success" => false, "message" => $errors]);
    exit;
}

$conn->begin_transaction();

$sql = "INSERT INTO product (barcode, title, price, supplier, stock, expire, category, quantity) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

$stmt = $conn->prepare($sql);
if (!$stmt) {
    echo json_encode(["success" => false, 'message' => 'Error preparing statement: ' . $conn->error]);
    exit;
}

$stmt->bind_param("ssssissi", $barcode, $title, $price, $supplier, $stock, $expire, $category, $quantity);
if (!$stmt->execute()) {
    $conn->rollback();
    echo json_encode(["success" => false, "message" => "Error: " . $stmt->error]);
    exit;
}

$productID = (int) $conn->insert_id;

$stmt->close();

foreach ($images as $imageID) {

    $sql_images = "INSERT INTO pic (productID, imageID) VALUES (?, ?)";

    $stmt_images = $conn->prepare($sql_images);
    if (!$stmt_images) {
        echo json_encode(["success" => false, 'message' => 'Error preparing statement: ' . $conn->error]);
        exit;
    }

    $stmt_images->bind_param("is", $productID, $imageID);
    if (!$stmt_images->execute()) {
        $conn->rollback();
        echo json_encode(["success" => false, "message" => "Error uploading image: " . $stmt_images->error]);
        exit;
    }
}

$stmt_images->close();

$conn->commit();
$conn->close();

echo json_encode(["success" => true, "message" => "New entry added successfully with images"]);
