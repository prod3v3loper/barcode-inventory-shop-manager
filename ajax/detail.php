<?php

header('Content-Type: application/json');

require_once '../root.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'config.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'error.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'db/conn.php';

$productId = $_GET['productId'] ?? 0;

$sql = "SELECT barcode, title, price, supplier, stock, expire, category, quantity FROM product WHERE id = ?";
$stmt = $conn->prepare($sql);
if (!$stmt) {
    echo json_encode(['success' => false, 'message' => 'Error preparing statement: ' . $conn->error]);
    exit;
}

$stmt->bind_param("i", $productId);
if (!$stmt->execute()) {
    echo json_encode(['success' => false, 'message' => 'Error executing statement: ' . $stmt->error]);
    exit;
}

$result = $stmt->get_result();
$product = $result->fetch_assoc();

$stmt->close();

$images = [];

if ($product) {

    $sql_images = "SELECT id, imageID FROM pic WHERE productID = ?";
    $stmt_images = $conn->prepare($sql_images);
    if (!$stmt_images) {
        echo json_encode(['success' => false, 'message' => 'Error preparing images statement: ' . $conn->error]);
        exit;
    }

    $stmt_images->bind_param("i", $productId);
    if ($stmt_images->execute()) {
        $result_images = $stmt_images->get_result();
        while ($row = $result_images->fetch_assoc()) {
            $images[] = [
                'id' => $row['id'],
                'imagePath' => PROJECT_HTTP_ROOT . DIRECTORY_SEPARATOR . $row['imageID']
            ];
        }
    }

    $stmt_images->close();
}

$response = [
    'success' => true,
    'product' => $product,
    'images' => $images
];

echo json_encode($response);

$conn->close();
