<?php

header('Content-Type: application/json');

require_once '../root.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'config.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'error.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'db/conn.php';

$productId = $_POST['productId'] ?? 0;

if ($productId == 0) {
    echo json_encode(['success' => false, 'message' => 'Invalid product ID.']);
    exit;
}

$conn->begin_transaction();

$sql = "SELECT imageID FROM pic WHERE productID = ?";

$stmt = $conn->prepare($sql);
if (!$stmt) {
    echo json_encode(['error' => 'Error preparing statement: ' . $conn->error]);
    exit;
}

$stmt->bind_param("i", $productId);
if (!$stmt->execute()) {
    echo json_encode(['error' => 'Error executing statement: ' . $stmt->error]);
    exit;
}

$result = $stmt->get_result();

$images = $result->fetch_all(MYSQLI_ASSOC);
foreach ($images as $image) {
    if (file_exists($image['imageID'])) {
        unlink($image['imageID']);
    }
}

$sql = "DELETE FROM pic WHERE productID = ?";

$stmt = $conn->prepare($sql);
$stmt->bind_param("i", $productId);
$stmt->execute();

$sql = "DELETE FROM product WHERE id = ?";

$stmt = $conn->prepare($sql);
if (!$stmt) {
    $conn->rollback();
    echo json_encode(['error' => 'Error preparing statement: ' . $conn->error]);
    exit;
}

$stmt->bind_param("i", $productId);
if ($stmt->execute()) {
    $conn->commit();
    echo json_encode(['success' => true, 'message' => 'Product and images successfully deleted.']);
} else {
    $conn->rollback();
    echo json_encode(['success' => false, 'message' => 'Error deleting product: ' . $stmt->error]);
}

$stmt->close();
$conn->close();
