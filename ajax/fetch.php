<?php

header('Content-Type: application/json');

require_once '../root.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'config.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'error.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'db/conn.php';

$sql = "SELECT p.*, pi.imageID 
        FROM product p
        LEFT JOIN pic pi ON p.id = pi.productID";
$result = $conn->query($sql);

$products = [];
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $productId = $row['id'];
        if (!isset($products[$productId])) {
            $products[$productId] = $row;
            $products[$productId]['images'] = [];
        }
        if (!empty($row['imageID'])) {
            $products[$productId]['images'][] = $row['imageID'];
        }
    }
}

if (count($products) > 0) {
    echo json_encode(array_values($products));
} else {
    echo json_encode(["success" => false, "message" => "No products"]);
}

$conn->close();
