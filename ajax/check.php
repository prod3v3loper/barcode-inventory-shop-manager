<?php

header('Content-Type: application/json');

require_once '../root.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'config.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'error.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'db/conn.php';

$barcode = isset($_POST['barcode']) && !empty($_POST['barcode']) ? $_POST['barcode'] : '';

if ($barcode) {

    $barcode = $_POST['barcode'];

    $sql = "SELECT * FROM product WHERE barcode = ?";

    $stmt = $conn->prepare($sql);
    if (!$stmt) {
        echo json_encode(['error' => 'Error preparing statement: ' . $conn->error]);
        exit;
    }

    $stmt->bind_param("s", $barcode);
    if (!$stmt->execute()) {
        echo json_encode(['error' => 'Error executing statement: ' . $stmt->error]);
        exit;
    }

    $result = $stmt->get_result();
    $exists = $result->num_rows > 0;

    echo json_encode(['exists' => $exists]);

    $stmt->close();
    $conn->close();
}
