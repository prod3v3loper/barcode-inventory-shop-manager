<?php

header('Content-Type: application/json');

require_once '../root.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'config.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'error.php';
require_once PROJECT_DOCUMENT_ROOT . DIRECTORY_SEPARATOR . 'db/conn.php';

$productId = $_POST['productId'] ?? 0;
$barcode = trim($_POST['barcode'] ?? '');
$title = trim($_POST['title'] ?? '');
$price = trim($_POST['price'] ?? '');
$supplier = trim($_POST['supplier'] ?? '');
$stock = trim($_POST['stock'] ?? '');
$expire = trim($_POST['expire'] ?? '');
$category = trim($_POST['category'] ?? '');
$quantity = intval($_POST['quantity'] ?? 0);
$images = [];

$errors = [];

// Validation
if (empty($barcode)) {
    $errors[] = "Barcode is required.";
}
if (empty($title)) {
    $errors[] = "Title is required.";
}
if (!empty($price) && !is_numeric($price)) {
    $errors[] = "Price must be a valid number.";
}
if (!empty($stock) && !is_numeric($stock)) {
    $errors[] = "Minimum stock must be a valid number.";
}
if (!empty($expire) && !preg_match("/^\d{4}-\d{2}-\d{2}$/", $expire)) {
    $errors[] = "Expiry date must be in YYYY-MM-DD format.";
}
if ($quantity <= 0) {
    $errors[] = "Quantity must be a non-negative integer.";
}

if (!empty($_FILES['images']['name'][0])) {
    $targetDir = "upload/";
    foreach ($_FILES['images']['name'] as $key => $value) {
        $targetFile = $targetDir . basename($_FILES['images']['name'][$key]);
        $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
        $check = getimagesize($_FILES['images']['tmp_name'][$key]);
        if ($check !== false) {
            if (move_uploaded_file($_FILES['images']['tmp_name'][$key], $targetFile)) {
                $images[] = $targetFile;
            } else {
                $errors[] = "Error uploading file: " . $value;
            }
        } else {
            $errors[] = "File is not an image: " . $value;
        }
    }
}

if (!empty($errors)) {
    echo json_encode(["success" => false, "errors" => $errors]);
    exit;
}

$conn->begin_transaction();

$sql = "UPDATE product SET barcode = ?, title = ?, price = ?, supplier = ?, stock = ?, expire = ?, category = ?, quantity = ? WHERE id = ?";

$stmt = $conn->prepare($sql);
if (!$stmt) {
    echo json_encode(['error' => 'Error preparing statement: ' . $conn->error]);
    exit;
}

$stmt->bind_param("ssssissii", $barcode, $title, $price, $supplier, $stock, $expire, $category, $quantity, $productId);
if (!$stmt->execute()) {
    $conn->rollback();
    echo json_encode(['success' => false, 'message' => 'Error updating product: ' . $stmt->error]);
}

$stmt->close();

if (count($images) > 0) {
    foreach ($images as $imageID) {

        $sql_images = "INSERT INTO pic (productID, imageID) VALUES (?, ?)";

        $stmt_images = $conn->prepare($sql_images);
        if (!$stmt_images) {
            echo json_encode(["success" => false, 'message' => 'Error preparing statement: ' . $conn->error]);
            exit;
        }

        $stmt_images->bind_param("is", $productID, $imageID);
        if (!$stmt_images->execute()) {
            $conn->rollback();
            echo json_encode(["success" => false, "message" => "Error uploading image: " . $stmt_images->error]);
            exit;
        }
    }

    $stmt_images->close();
}

echo json_encode(['success' => true, 'message' => 'Product successfuly updated']);

$conn->commit();
$conn->close();
