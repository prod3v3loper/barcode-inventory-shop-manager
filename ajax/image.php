<?php

if (isset($_POST['imageId'])) {

    $imageId = $_POST['imageId'];

    $stmt = $conn->prepare("SELECT imageID FROM pic WHERE id = ?");
    $stmt->bind_param("i", $imageId);
    $stmt->execute();
    $result = $stmt->get_result();
    $imagePath = $result->fetch_assoc()['imageID'];

    if (file_exists($imagePath)) {
        unlink($imagePath);
    }

    $stmt = $conn->prepare("DELETE FROM pic WHERE id = ?");
    $stmt->bind_param("i", $imageId);
    $stmt->execute();

    echo json_encode(['success' => true]);
}
